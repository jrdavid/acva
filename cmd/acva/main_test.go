package main

import (
	"bytes"
	"testing"
)

func TestMain(t *testing.T) {
	exitFunction = func(code int) {}
	in = bytes.NewBuffer([]byte("foobar"))
	out = bytes.NewBuffer(nil)
	args = []string{"placeholder", "--info", "test-vectors/name.toml"}
	wanted := "foobar"
	main()
	if actual := out.(*bytes.Buffer).String(); actual != wanted {
		t.Errorf("Wanted %q, got %q", wanted, actual)
	}
}

func TestMainWithName(t *testing.T) {
	exitFunction = func(code int) {}
	in = bytes.NewBuffer([]byte(">{{ .Name }}<"))
	out = bytes.NewBuffer(nil)
	args = []string{"placeholder", "--info", "test-vectors/name.toml"}
	wanted := ">my-test-name<"
	main()
	if actual := out.(*bytes.Buffer).String(); actual != wanted {
		t.Errorf("Wanted %q, got %q", wanted, actual)
	}
}

func TestMainWithNameReadFromTemplateFile(t *testing.T) {
	exitFunction = func(code int) {}
	out = bytes.NewBuffer(nil)
	args = []string{"placeholder", "--info", "test-vectors/name.toml", "test-vectors/name.template"}
	wanted := ">my-test-name<\n"
	main()
	if actual := out.(*bytes.Buffer).String(); actual != wanted {
		t.Errorf("Wanted %q, got %q", wanted, actual)
	}
}
