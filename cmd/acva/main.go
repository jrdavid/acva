package main

import (
	"flag"
	"fmt"
	"io"
	"os"

	"gitlab.com/jrdavid/acva/pkg/renderers"
	"gitlab.com/jrdavid/acva/pkg/repositories"
)

var in io.Reader = os.Stdin
var out io.Writer = os.Stdout
var args = os.Args
var exitFunction = func(code int) { os.Exit(code) }

func main() {
	flagSet := flag.NewFlagSet("options", flag.ExitOnError)
	dataFile := flagSet.String("info", "info.toml", "Personal information file")
	flagSet.Parse(args[1:])

	if flagSet.NArg() > 0 {
		inFile, err := os.Open(flagSet.Arg(0))
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err)
			exitFunction(1)
		}
		in = inFile
	}
	source, _ := os.Open(*dataFile)
	repository := repositories.TOMLRepository{TOMLSource: source}
	renderer := renderers.RendererStandard{Repo: repository}
	err := renderer.Render(in, out)
	if err != nil {
		exitFunction(1)
	}
	exitFunction(0)
}
