package repositories

import (
	"bytes"
	"errors"
	"reflect"
	"testing"
)

func TestTOMLRepoReturnsResumeData(t *testing.T) {
	source := &bytes.Buffer{}
	repo := TOMLRepository{TOMLSource: source}
	data, err := repo.Data()
	dataType := reflect.TypeOf(data)
	if dataType.String() != "renderers.ResumeData" {
		t.Errorf("Got %q", dataType.String())
	}
	if err != nil {
		t.Errorf("Got error %q, expected success", err)
	}
}

func TestTOMLRepoEmptySourceReturnsEmptyData(t *testing.T) {
	source := &bytes.Buffer{}
	repo := TOMLRepository{TOMLSource: source}
	data, err := repo.Data()
	if data.Name != "" {
		t.Errorf("Unexpected data %q", data.Name)
	}
	if err != nil {
		t.Errorf("Got error %q, expected success", err)
	}
}

func TestTOMLRepoSourceWithNameReturnsDataWithName(t *testing.T) {
	source := &bytes.Buffer{}
	source.Write([]byte("name = \"my-name\"\n"))
	repo := TOMLRepository{TOMLSource: source}
	data, err := repo.Data()
	if data.Name != "my-name" {
		t.Errorf("Got %q, wanted %q", data.Name, "my-name")
	}
	if err != nil {
		t.Errorf("Got error %q, expected success", err)
	}
}

type ReaderError struct{}

func (r ReaderError) Read(b []byte) (n int, err error) {
	return 0, errors.New("Simulated Read Error")
}

func TestTOMLRepoErrorSourceReturnsError(t *testing.T) {
	readerError := ReaderError{}
	repo := TOMLRepository{TOMLSource: readerError}
	_, err := repo.Data()
	if err == nil {
		t.Errorf("Got success, wanted error")
	}
}

func TestTOMLRepoInvalidTOMLReturnsError(t *testing.T) {
	source := &bytes.Buffer{}
	source.Write([]byte("name="))
	repo := TOMLRepository{TOMLSource: source}
	_, err := repo.Data()
	if err == nil {
		t.Errorf("Got success, wanted error")
	}
}
