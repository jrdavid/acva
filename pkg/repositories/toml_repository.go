package repositories

import (
	"io"
	"io/ioutil"

	"github.com/BurntSushi/toml"

	"gitlab.com/jrdavid/acva/pkg/renderers"
)

type TOMLRepository struct {
	TOMLSource io.Reader
	resumeData renderers.ResumeData
}

func (r TOMLRepository) Data() (renderers.ResumeData, error) {
	tomlBytes, err := ioutil.ReadAll(r.TOMLSource)
	if err != nil {
		return renderers.ResumeData{}, err
	}
	tomlString := string(tomlBytes)
	_, err = toml.Decode(tomlString, &r.resumeData)
	if err != nil {
		return renderers.ResumeData{}, err
	}
	return r.resumeData, nil
}
