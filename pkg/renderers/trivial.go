package renderers

import (
	"io"
	"io/ioutil"
)

type Trivial struct{}

func (*Trivial) Render(source io.Reader, destination io.Writer) error {
	template, err := ioutil.ReadAll(source)
	if err != nil {
		return err
	}
	_, err = destination.Write(template)
	if err != nil {
		return err
	}
	return nil
}
