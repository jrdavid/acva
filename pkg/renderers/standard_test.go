package renderers

import (
	"bytes"
	"testing"
)

type EmptyRepository struct{}

func (nr EmptyRepository) Data() (ResumeData, error) {
	return ResumeData{}, nil
}

func TestGivenEmptyDataAndEmptyTemplateThenOutputIsEmpty(t *testing.T) {
	source := bytes.Buffer{}
	destination := bytes.Buffer{}
	emptyRepository := EmptyRepository{}
	renderer := RendererStandard{Repo: emptyRepository}
	err := renderer.Render(&source, &destination)
	if err != nil {
		t.Errorf("Got unexpected error %v", err)
	}
	if destination.Len() > 0 {
		t.Errorf("Got output on empty input")
	}
}

func TestGivenEmptyDataAndValidTemplateThenOutputEqualsInput(t *testing.T) {
	templateString := []byte("foobar")
	source := bytes.Buffer{}
	source.Write(templateString)
	destination := bytes.Buffer{}
	emptyRepository := EmptyRepository{}
	renderer := RendererStandard{Repo: emptyRepository}
	err := renderer.Render(&source, &destination)
	if err != nil {
		t.Errorf("Got unexpected error %v", err)
	}
	if bytes.Compare(templateString, destination.Bytes()) != 0 {
		t.Errorf("Wanted %q, got %q", source.Bytes(), destination.Bytes())
	}
}

type NameRepository struct{}

func (nr NameRepository) Data() (ResumeData, error) {
	return ResumeData{Name: "foo"}, nil
}

func TestGivenDataAndValidTemplateThenOutputRendersData(t *testing.T) {
	templateString := []byte(">{{ .Name }}<")
	source := bytes.Buffer{}
	source.Write(templateString)
	nameRepository := NameRepository{}
	destination := bytes.Buffer{}
	renderer := RendererStandard{Repo: nameRepository}
	err := renderer.Render(&source, &destination)
	if err != nil {
		t.Errorf("Got unexpected error %v", err)
	}
	want := ">foo<"
	if bytes.Compare([]byte(want), destination.Bytes()) != 0 {
		t.Errorf("Wanted %q, got %q", want, destination.Bytes())
	}
}
