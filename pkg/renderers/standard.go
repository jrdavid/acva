package renderers

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"text/template"
)

type Item struct {
	En string
	Fr string
}

type Job struct {
	Company          string
	Start            string
	End              string
	Title            Item
	Environment      []Item
	Responsibilities []Item
}

type Degree struct {
	Name        Item
	Institution string
	Date        string
}

type Hobby struct {
	Name         Item
	Achievements []Item
}

type SocialMedia struct {
	Name string
	URL  string
	Show bool
}

type ResumeData struct {
	Name         string
	Email        string
	Phone        string
	Objectives   []Item
	Competencies []Item
	LastJob      Job
	Jobs         []Job
	Education    []Degree
	Hobbies      []Hobby
	SocialMedias []SocialMedia
}

type Repository interface {
	Data() (ResumeData, error)
}

type RendererStandard struct {
	Repo Repository
}

func (r *RendererStandard) Render(source io.Reader, destination io.Writer) error {
	templateText, err := ioutil.ReadAll(source)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v", err)
		return err
	}
	goTemplate := template.Must(template.New("template_name").Parse(string(templateText)))
	resumeData, _ := r.Repo.Data()
	err = goTemplate.Execute(destination, resumeData)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v", err)
		return err
	}
	return nil
}
