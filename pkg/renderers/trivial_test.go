package renderers

import (
	"bytes"
	"testing"
)

func TestTrivialRendererRendersEmptyTemplateToEmptyString(t *testing.T) {
	source := bytes.Buffer{}
	destination := bytes.Buffer{}
	trivial := Trivial{}
	trivial.Render(&source, &destination)
	if destination.Len() > 0 {
		t.Errorf("Got output on empty input")
	}
}

func TestTrivialRendererRendersInputToOutputWithoutChange(t *testing.T) {
	data := []byte("foobar")
	source := bytes.Buffer{}
	source.Write(data)
	destination := bytes.Buffer{}
	trivial := Trivial{}
	trivial.Render(&source, &destination)
	if bytes.Compare(data, destination.Bytes()) != 0 {
		t.Errorf("Wanted %q, got %q", source.Bytes(), destination.Bytes())
	}
}
